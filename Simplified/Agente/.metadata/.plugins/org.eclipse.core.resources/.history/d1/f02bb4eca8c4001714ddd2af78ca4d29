
# for printing Q and P tables
import pprint
pp = pprint.PrettyPrinter(depth=100)

# random methods for action selection and state initialization
from random import random
from random import choice

# learning variables
from exp import *

# online methods of oASP(MDP)
import online

# ASP methods of oASP(MDP)
import asp

# MDP methods of oASP(MDP)
import mdp

# episode variables
tSteps = 0   # total steps in a episode
actions = [] # actions list

maximumReward = 0     # maximum value for reward
minimumReward = 0     # minimum value for reward

forbiddenStates = []  # list of forbidden states
forbiddenActions = []  # list of forbidden actions
forbiddenActionState = [] # list of forbidden state-action pairs
goalState = []  # list of goal states


################################################################################
def saveData(qs, diffQ, steps, ret, trial):
    from pickle import dump
    from os import popen

    fname = 'diffQ-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(diffQ, f)

    fname = 'steps-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(steps, f)

    fname = 'ret-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(ret, f)

    fname = 'qsa-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(qs, f)

    popen('rm *.lp')


################################################################################
def episode(qTable, pTable):

    global tSteps
    global minimumReward, maximumReward, actions

    # get connection to environment
    socket = online.initSocket(port)

    # receive env information
    maximumReward, minimumReward, actions = online.getConf(socket)

    # save actions for ASP calls
    if RUN_ASP:
        asp.saveActionList(actions, forbiddenActions)

    # agent's initial configuration
    newState, end = online.initState(socket)

    # variables to save
    step = 0
    returnReward = 0

    somethingChanged = False

    # here is the oASP(MDP)
    while not end:

        ### ONLINE
        # update state
        currentState = newState
        
        # choose action
        actionToPerform = mdp.chooseAction(qTable, actions, currentState, 
                                           choice, forbiddenActions, forbiddenActionState)

        # perform action in the environment
        online.sendMessage(currentState, actionToPerform, socket)

        # receive information from environment
        newState, reward, end = online.receiveMessage(socket)

        ### END ONLINE

        # UPDATE Q AND P FUNCTIONS AND ASP FILES
        changes = asp.updateASPfiles(qTable, pTable,
                currentState, actionToPerform, newState,
                reward, minimumReward, maximumReward,
                goalState, forbiddenStates, forbiddenActionState)
        somethingChanged = somethingChanged or changes
        # END UPDATE

        ### MDP
        # Q(s,a) update

        qTable[currentState][actionToPerform] = mdp.updateQvalue(qTable, currentState, 
                                                                 actionToPerform, reward, newState)

        
        # variables to send
        step += 1
        returnReward += reward 

        tSteps += 1
        if step == maxSteps:
            end = True
        ### END MDP

        ### ASP CALL
        if RUN_ASP:
            if changes:
                asp.saveStateDesc(pTable, currentState)
                #print("Something changed...")


        ### END ASP CALL

        if DEBUG: input("Press anykey to continue")


    online.sendEndEpisode(socket)

    if somethingChanged:
        #print("Q ASP update")
        newQTableFromASP = asp.genNewQ(pTable)

        if DEBUG: pp.pprint(newQTableFromASP)
        qTable = mdp.updateQwithASP(qTable, newQTableFromASP)

    return qTable, pTable, step, returnReward


################################################################################
def experiment():

    for trial in range(trials):
        print("Trial: %s" % int(trial+1))

        pTable = dict()
        qTable = dict()

        # variables to be saved
        qTablesList = list()
        steps = list()
        returns = list()
        diffQ = list()
        oldQTable = qTable.copy()

        for currentEpisode in range(nenvs * episodes):
            print("Episode: %s" % int(currentEpisode))
            qTable, pTable, step, reward = episode(qTable, pTable)

            if currentEpisode % window == 0:
                differenceBetweenQs = mdp.rmsd(oldQTable, qTable)
                oldQTable = qTable.copy()
                qTablesList.append(oldQTable)
                steps.append(step)
                returns.append(reward)
                diffQ.append(differenceBetweenQs)

        #print("Step: %s" % tSteps)

        #print("\n\nP(s,a,s)")
        #pp.pprint(p)

        print("*"*30)
        print("\n\nQ(s,a)")
        pp.pprint(qTable)
        print("*"*30)
        #input()

        saveData(qTablesList, diffQ, steps, returns, trials+1)

    print("\n\n")
    print("Tabela Final " + str(qTable))


################################################################################
experiment()
