import asp2py
import py2asp

from exp import RUN_ASP, DETERMINISTIC


################################################################################
def updateASPfiles(qTable, pTable, currentState, action, newState,
        reward, minimumReward, maximumReward,
        goalStates, forbiddenStates, forbiddenActionState):

    somethingChanged = False
    # update restriction list

    # forbidden state-action pairs
    ## criteria: transition to the same state
    if DETERMINISTIC:
        if (currentState == newState or reward == minimumReward) and [currentState, action] not in forbiddenActionState:
            forbiddenActionState.append([currentState, action])
            for proibidos in forbiddenActionState:
                print("proibido: " + str(proibidos) + "\n")
            if RUN_ASP:
                py2asp.restrictSApairs(forbiddenActionState, currentState)
            somethingChanged = True

    # forbidden actions
    # criteria: ???
    #TODO: How to determine a forbidden Action?

    # forbidden states
    ## criteria: minimum reward
    #this code is comented because I wanted to test something first
    '''if reward == minimumReward and newState not in forbiddenStates or True:
        forbiddenStates.append(newState)
        if RUN_ASP:
            py2asp.restrictStates(forbiddenStates)
        somethingChanged = True
'''
    # save goal state
    ## criteria: maximum reward
    if reward == maximumReward and newState not in goalStates:
        goalStates.append(newState)
        if RUN_ASP:
            py2asp.saveGoalStates(goalStates)
        somethingChanged = True

    # update Q and P functions
    if currentState not in pTable:
        pTable[currentState] = dict()
        somethingChanged = True

    if currentState not in qTable:
        qTable[currentState] = dict()
        somethingChanged = True

    if action not in pTable[currentState]:
        pTable[currentState][action] = list()
        somethingChanged = True

    if action not in qTable[currentState]:
        qTable[currentState][action] = 0
        somethingChanged = True

    if newState not in pTable[currentState][action]:
        pTable[currentState][action].append(newState)
        somethingChanged = True

    if somethingChanged:
        if RUN_ASP:
            py2asp.psa2lp(pTable, currentState)

    return somethingChanged


################################################################################
def genNewQ(pTable):
    #print("genNewQ")
    return asp2py.genQfromLP(pTable.keys())


################################################################################
def saveActionList(actions, forbiddenActions):
    py2asp.saveActions(actions)
    py2asp.restrictActions(forbiddenActions)


################################################################################
def saveStateDesc(pTable, state):
    py2asp.psa2lp(pTable, state)


################################################################################
