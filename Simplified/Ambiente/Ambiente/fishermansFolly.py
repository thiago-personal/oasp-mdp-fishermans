### Fisherman's Folly Puzzle

#Imports
from enviromentDefinition import *
from random import random
import zmq
import enviromentDefinition
from enumerations import ElementFace, ElementType
from ElementsRelation import ElementsRelation

################################################################################
def nextState(state, action, elementFace):
    terminalState = False
    reward  = rewardPerStep
    forbiddenStateActionPerformed = False
    
    #Getting the mapped state and casting to the complex state, with the three lists.
    currentState = mapFromStringToState(state)
    stringRelations, postRelations, ringRelations = currentState
    
    #Show informations about each relation of the current state of the puzzle. 
    #showInformationAboutRelations(currentState)
        
    if action == Actions.PostThroughRing.value:
        if not elementIsBeingCrossedBy(ring.elementType.value, postRelations):
            relationToAdd = ElementsRelation(ElementType.POST, ring.elementType.value, ElementFace.POSITIVE)
            postRelations.append(relationToAdd)
            
            #Esse código é um teste, uma tentativa para ver o que acontece.
            if elementIsBeingCrossedBy(ring.elementType.value, stringRelations):
                relationToRemove = ElementsRelation(ElementType.MAINSTRING, ring.elementType.value, ElementFace.POSITIVE)
                stringRelations = removeElementFromRelations(stringRelations, relationToRemove)
        else:
            forbiddenStateActionPerformed = True
    
    elif action == Actions.PostFromRing.value:
        if elementIsBeingCrossedBy(ring.elementType.value, postRelations):
            #Removing the relation from the POST
            relationToRemove = ElementsRelation(ElementType.POST, ring.elementType.value, ElementFace.POSITIVE)
            postRelations = removeElementFromRelations(postRelations, relationToRemove)
            
            #Adding the relation between the STRING and the RING
            if not elementIsBeingCrossedBy(ring.elementType.value, stringRelations):
                relationToAdd = ElementsRelation(ElementType.MAINSTRING, ring.elementType.value, ElementFace.POSITIVE)
                stringRelations.append(relationToAdd)
        else:
            forbiddenStateActionPerformed = True          
            
    elif action == Actions.StringThroughRing.value:
        if (not elementIsBeingCrossedBy(ring.elementType.value, stringRelations)  and
           (elementIsBeingCrossedBy(disk1.elementType.value, postRelations) or
            elementIsBeingCrossedBy(disk2.elementType.value, postRelations))):
            relationToAdd = ElementsRelation(ElementType.MAINSTRING, ring.elementType.value, ElementFace.POSITIVE)
            stringRelations.append(relationToAdd)
            
            if elementIsBeingCrossedBy(ring.elementType.value, postRelations):
                relationToRemove = ElementsRelation(ElementType.POST, ring.elementType.value, ElementFace.POSITIVE)
                postRelations = removeElementFromRelations(postRelations, relationToRemove)
            
        else:
            forbiddenStateActionPerformed = True
        #forbiddenStateActionPerformed = True
            
    elif action == Actions.StringFromRing.value:
        if (elementIsBeingCrossedBy(ring.elementType.value, stringRelations) and
           elementIsBeingCrossedBy(post.elementType.value, ringRelations)):
            relationToRemove = ElementsRelation(ElementType.MAINSTRING, ring.elementType.value, ElementFace.POSITIVE)
            stringRelations = removeElementFromRelations(stringRelations, relationToRemove)
        else:
            forbiddenStateActionPerformed = True 
        #forbiddenStateActionPerformed = True
    
    
    elif action == Actions.Disk1ThroughPost.value:
        if (not elementIsBeingCrossedBy(disk1.elementType.value, postRelations) and
            not elementIsBeingCrossedBy(disk2.elementType.value, postRelations) and
            not elementIsBeingCrossedBy(ring.elementType.value, stringRelations)):
            relationToAdd = ElementsRelation(ElementType.POST, disk1.elementType.value, ElementFace.POSITIVE)
            postRelations.append(relationToAdd)
        else:
            forbiddenStateActionPerformed = True 
            
    elif action == Actions.Disk1FromPost.value:
        if (elementIsBeingCrossedBy(disk1.elementType.value, postRelations) and
           not elementIsBeingCrossedBy(ring, stringRelations)):
            relationToRemove = ElementsRelation(ElementType.POST, disk1.elementType.value, ElementFace.POSITIVE)
            postRelations = removeElementFromRelations(postRelations, relationToRemove)
        else:
            forbiddenStateActionPerformed = True 
            
            
    elif action == Actions.Disk2ThroughPost.value:
        if (not elementIsBeingCrossedBy(disk2.elementType.value, postRelations) and 
            not elementIsBeingCrossedBy(disk1.elementType.value, postRelations) and
            not elementIsBeingCrossedBy(ring.elementType.value, stringRelations)):
            relationToAdd = ElementsRelation(ElementType.POST, disk2.elementType.value, ElementFace.POSITIVE)
            postRelations.append(relationToAdd)
        else:
            forbiddenStateActionPerformed = True 
    
    elif action == Actions.Disk2FromPost.value:
        if (elementIsBeingCrossedBy(disk2.elementType.value, postRelations) and
           not elementIsBeingCrossedBy(ring, stringRelations)):
            relationToRemove = ElementsRelation(ElementType.POST, disk2.elementType.value, ElementFace.POSITIVE)
            postRelations = removeElementFromRelations(postRelations, relationToRemove)
        else:
            forbiddenStateActionPerformed = True 
            
            
    elif action == Actions.RingThroughPost.value:
        if ((not elementIsBeingCrossedBy(ring.elementType.value, postRelations)) and 
        (elementIsBeingCrossedBy(disk1.elementType.value, postRelations) or
         elementIsBeingCrossedBy(disk2.elementType.value, postRelations)) and 
        (not elementIsBeingCrossedBy(post.elementType.value, ringRelations))):
            relationToAdd = ElementsRelation(ElementType.RING, post.elementType.value, ElementFace.POSITIVE)
            ringRelations.append(relationToAdd)
        else:
            forbiddenStateActionPerformed = True 
            
    elif action == Actions.RingFromPost.value:
        if elementIsBeingCrossedBy(post.elementType.value, ringRelations):
            relationToRemove = ElementsRelation(ElementType.RING, post.elementType.value, ElementFace.POSITIVE)
            ringRelations = removeElementFromRelations(ringRelations, relationToRemove)
        else:
            forbiddenStateActionPerformed = True 
    
    
    newState = (stringRelations.copy(), postRelations.copy(), ringRelations.copy())
    mappedNewState = mapStateToString(newState)
    #print("New state mapped: " + mappedNewState)
    
    if (forbiddenStateActionPerformed):
        reward = minimalReward 
        
    elif isGoalState(newState):
        reward = maximumReward
        terminalState = True
    
    '''elif newStateMappedInString == "p3bbp1":
        reward = maximumReward
        terminalState = True
    '''
        
    #showInformationAboutRelations(newState)
    
    return mappedNewState, reward, terminalState

################################################################################
def removeElementFromRelations(listOfRelations, relationToRemove):
    indexToRemove = -1
    #Get the last element of the listOfRelations to remove.
    for indexAux, item in enumerate(listOfRelations):
        if(int(item.parent.value) == int(relationToRemove.parent.value) and
           int(item.element) == int(relationToRemove.element) and
           int(item.face.value) == int(relationToRemove.face.value)):
            indexToRemove = indexAux

    '''Olhar melhor essa função de remoção.
    Por exemplo, para remover o anel do post, está passando a face como negativa, mas o anel tá com a face
        positiva nas relações com o poste. Rever o critério para exclusão de relação.
'''
    #for item in listOfRelations:
    #    print("Item antes de remocao: " + item.element)

    if(indexToRemove >= 0):
        del listOfRelations[indexToRemove]
        #print("Element removed!")
    else:
        print("Elements Relations not found!")
        
    
    #for item in listOfRelations:
    #    print("Item depois de remocao: " + item.element)
        
    return listOfRelations        

################################################################################
def mapStateToString(stateToMap):
    mappedState = ""
    
    #Separating the two list of crossings, one for each element of the system.
    stringCrossings, postCrossings, ringCrossings = stateToMap 
    
    for elementRelationToConvert in stringCrossings:
        mappedState += getElementStateRepresentation(elementRelationToConvert)
    
    #mappedState += "-"
    mappedState += separador
    
    for elementRelationToConvert in postCrossings:
        mappedState += getElementStateRepresentation(elementRelationToConvert)
        
    #mappedState += "-"
    mappedState += separador
    
    for elementRelationToConvert in ringCrossings:
        mappedState += getElementStateRepresentation(elementRelationToConvert)
        
    
    return mappedState

################################################################################
def mapFromStringToState(mappedState):
    #print(mappedState)
    #stringCrossingsMapped, postCrossingsMapped, ringCrossingsMapped = mappedState.split("-")
    stringCrossingsMapped, postCrossingsMapped, ringCrossingsMapped = mappedState.split(separador)
    
    stringCrossings = getComplexRelationReprsentation(stringCrossingsMapped, ElementType.MAINSTRING)
    postCrossings = getComplexRelationReprsentation(postCrossingsMapped, ElementType.POST)
    ringCrossings = getComplexRelationReprsentation(ringCrossingsMapped, ElementType.RING)
    
    complexState = (stringCrossings, postCrossings, ringCrossings)
    
    return complexState


################################################################################
def getElementStateRepresentation(elementRelationToConvert):
    #Getting the int value of the enumeration that represents the element that is crossing the string or the post.
    #print(str(elementRelationToConvert.element) + " " + str(elementRelationToConvert.face) + " " + str(elementRelationToConvert.parent))
    
    elementConverted = ""
    
    if elementRelationToConvert.face == ElementFace.POSITIVE:
        elementConverted = "p"
    else:
        elementConverted = "n"
        
    elementConverted += str(elementRelationToConvert.element)
        
    return elementConverted    

################################################################################
def getComplexRelationReprsentation(mappedStateToConvert, parent):
    numberOfPositionsToSplit = 2
    listOfElementsRelationMapped = [mappedStateToConvert[i:i+numberOfPositionsToSplit] 
                                   for i in range(0, len(mappedStateToConvert), numberOfPositionsToSplit)]
    elementRelations = list()
    
    for relation in listOfElementsRelationMapped:
        face = ElementFace.POSITIVE if relation[0] == "p" else ElementFace.NEGATIVE
        elementRelations.append(ElementsRelation(parent, relation[1], face))
        
    return elementRelations


################################################################################
def elementIsBeingCrossedBy(elementBeingCrossed, listOfElementCrossings):
    for relation in listOfElementCrossings:
        if(int(relation.element) == elementBeingCrossed):
            return True
        
    return False


################################################################################
def showInformationAboutRelations(stateToShowInformation):
    stringRelations, postRelations, ringRelations = stateToShowInformation
    
    for item in stringRelations:
        print("Element2: " + str(item.element) + ", Parent: " + str(item.parent) + ", Face: " + str(item.face))
        
    for item in postRelations:
        print("Element2: " + str(item.element) + ", Parent: " + str(item.parent) + ", Face: " + str(item.face))
    
    for item in ringRelations:
        print("Element: " + str(item.element) + ", Parent: " + str(item.parent) + ", Face: " + str(item.face))


################################################################################
#def findElementInRelations(listOfRelations, elementsRelationToFind):


################################################################################
def initState():
    #message = 'InitialState'
    message = mapStateToString(initialState)
    return message


################################################################################
def envConf():
    message = ''

    for action in actions:
        message += str(action.value) + ' '

    message += str(maximumReward) + ' ' + str(minimalReward)

    return message


################################################################################
def stationaryEnvironment(socket, maxEpisodes):
    print("FisherMan's Folly Puzzle")

    e = 0
    for _ in range(maxEpisodes):
        end = False

        while not end:
            message = socket.recv_string()
            #print("Phase: %s" % message)

            if message == 'init':
                message = initState()

            elif message == 'conf':
                message = envConf()
                e += 1

            elif message == 'END':
                end = True

            else:
                #The action to be performed came from the agent. The agent code decides the action.
                state, action = message.split()
                action = int(action)

                newState, reward, terminalState = nextState(state, action, 1) #Change this last argument
                printOnScreen  = 'Action Performed: ' + str(actions[action]) + '\n'
                printOnScreen += 'Reward: ' + str(reward)  + '\n'
                printOnScreen += 'Terminal State: ' + str(terminalState)
                print(printOnScreen)
                
                message = str(newState) + ' '
                message += str(reward) + ' '
                message += str(terminalState)

            #print("About this execution: %s" % message)
            socket.send_string(message)

################################################################################
def runEnvironment(episodes, trials):
    import zmq
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    conn = "tcp://*:" + str(port)
    socket.bind(conn)
    print("Environment is up")

    #global postRelations, stringRelations


    for t in range(trials):
        print("Trial: ", t)
        stringRelations = initialState[0]
        postRelations = initialState[1]
        stationaryEnvironment(socket, episodes)


################################################################################
if __name__ == '__main__':
    runEnvironment(episodes, trials)


################################################################################
