import matplotlib.pyplot as plt
import numpy as np
import pickle
from matplotlib.font_manager import FontProperties
from scipy.stats import ttest_ind
from scipy.stats import t

#T Test must be made between only two different algorithms, no more than two algorithms.

numberOfTrials = 30
splitCharacter = "-"
allFoldersNames = list()

#Rope Ladder Complete
#allFoldersNames.append("RopeLadderCompleto-oASPMDP/")
allFoldersNames.append("RopeLadderCompleto-oASPMDPHeuristic/")
#allFoldersNames.append("RopeLadderCompleto-QLearning/")
allFoldersNames.append("RopeLadderCompleto-QLearningHeuristic/")

#FishermansFolly Deterministic Complete
#allFoldersNames.append("FishermansFollyCompletoDeterministic-oASPMDP/")
#allFoldersNames.append("FishermansFollyCompletoDeterministic-oASPMDPHeuristic/")
#allFoldersNames.append("FishermansFollyCompletoDeterministic-QLearning/")
#allFoldersNames.append("FishermansFollyCompletoDeterministic-QLearningHeuristic/")

#FishermansFolly Deterministic Simplified
'''allFoldersNames.append("FishermansFollySimplifiedDeterministic-oASPMDP/")
allFoldersNames.append("FishermansFollySimplifiedDeterministic-QLearning/")'''

#FishermansFolly NonDeterministic Complete
#allFoldersNames.append("FishermansFollyNonDeterministic-oASPMDP/")
#allFoldersNames.append("FishermansFollyNonDeterministic-oASPMDPHeuristic/")
#allFoldersNames.append("FishermansFollyNonDeterministic-QLearning/")
#allFoldersNames.append("FishermansFollyNonDeterministic-QLearningHeuristic/")

#FishermansFolly NonDeterministic Simplified
'''allFoldersNames.append("FishermansFollySimplifiedNonDeterministic-oASPMDP/")
allFoldersNames.append("FishermansFollySimplifiedNonDeterministic-QLearning/")'''

#FishermansFolly NonStationary Sphere
'''allFoldersNames.append("FishermansFollyNonStationarySphere-oASPMDP/")
allFoldersNames.append("FishermansFollyNonStationarySphere-QLearning/")'''

#FishermansFolly NonStationary Disk
'''allFoldersNames.append("FishermansFollyNonStationaryDisk-oASPMDP/")
allFoldersNames.append("FishermansFollyNonStationaryDisk-QLearning/")'''

def mountGraph(valuesToShow, graphLabel, listOfGraphLegends, xLabel="X", yLabel="Y"):
    tamanhoVerdadeiro = len(valuesToShow)  # * 10

    valuesToShow = valuesToShow[::10]
    numberOfDataPoints = range(0, tamanhoVerdadeiro, 10)

    listOfGraphLegends[0] = "oASP(MDP) Heuristic VS QLearning Heuristic"
    #listOfGraphLegends[0] = "oASP(MDP) Heuristic VS oASP(MDP)"
    #listOfGraphLegends[0] = "oASP(MDP) VS QLearning"
    plt.scatter(numberOfDataPoints, valuesToShow, label=listOfGraphLegends[0])
    plt.xlim(-100, 6100)  # apply the x-limits

    alpha = 0.05
    #degreesOfFreedom = (2 * numberOfTrials) - 2
    degreesOfFreedom = (numberOfTrials) - 2
    criticalValue = t.ppf(1.0 - alpha, degreesOfFreedom)

    plt.axhline(criticalValue, color='r', label="5% Confidence Level")

    plt.xlabel(xLabel, fontsize=20)
    plt.ylabel(yLabel, fontsize=20)

    fontP = FontProperties()
    fontP.set_size('small')
    plt.title(graphLabel, fontsize=20)
    plt.legend(loc=1, prop={'size': 12})
    plt.savefig('tTest1.png')
    plt.show()


def getValues(baseFileName, folderName):
    listOfValues = list()
    baseFileName = folderName + baseFileName

    for trial in range(0, numberOfTrials):
        fullFileName = baseFileName + "-" + str(trial + 1) + ".pck"
        listOfValues.append(pickle.load(open(fullFileName, "rb")))

    return listOfValues


def getValuesByTrial(baseFileName, folderName, currentTrial):
    listOfValues = list()
    baseFileName = folderName + baseFileName

    fullFileName = baseFileName + "-" + str(currentTrial + 1) + ".pck"
    listOfValues.append(pickle.load(open(fullFileName, "rb")))

    return listOfValues


def getStatisticalValues(listOfValues, numberOfDataPoints):
    averageValuesAux = list()
    standardDeviationValuesAux = list()

    for i in range(0, numberOfDataPoints):
        valuesCombined = list()
        for j in range(0, numberOfTrials):
            valuesCombined.append(listOfValues[j][i])

        averageValuesAux.append(np.mean(valuesCombined))
        standardDeviationValuesAux.append(np.std(valuesCombined, axis=0, ddof=1))

    statisticalValues = (averageValuesAux, standardDeviationValuesAux)
    return statisticalValues

def getTTestValues(listOfValues, numberOfDataPoints):
    tTestValuesAux = list()

    for i in range(0, numberOfDataPoints):
        valuesCombined = list()
        for j in range(0, numberOfTrials):
            valuesCombined.append(listOfValues[j][i]) #Contains all the values for that interaction with all the 30trials.

        tTestValuesAux.append(valuesCombined)

    return tTestValuesAux

def getTTest(metricName, yLabel):
    baseFileName = metricName
    allData = list()
    listOfNames = list()

    for folderName in allFoldersNames:
        testingValues = getValues(baseFileName, folderName)

        numberOfDataPoints = len(testingValues[0])
        tTestValues = getTTestValues(testingValues, numberOfDataPoints)
        allData.append(tTestValues) #Putting all values in this variable for comparison in the T Test

        if (splitCharacter != ""):
            name = folderName.split(splitCharacter)[1]
            listOfNames.append(name)
        else:
            listOfNames.append("Teste")

    valuesToShow = list()
    numberOfInteractions = len(allData[0])
    for i in range(0, numberOfInteractions):
        dataFirstAlgorithm = allData[0][i]
        dataSecondAlgorithm = allData[1][i]

        tStatisticValue, pValue = ttest_ind(dataFirstAlgorithm, dataSecondAlgorithm, equal_var=False)
        if(abs(tStatisticValue) > 30):
            tStatisticValue = 30
        valuesToShow.append(abs(tStatisticValue))

    graphLabel = "Student's T Test"
    xLabel = "Interaction"
    mountGraph(valuesToShow, graphLabel, listOfNames, xLabel, yLabel)


def getTTestNumberOfSteps():
    getTTest("steps", "Difference")

def getTTestReturnValues():
    getTTest("ret", "Difference")

def getTTestNumberOfStates():
    getTTest("qsa", "Number of States")

def getTTestNumberOfStatesActionsPair():
    getTTest("stateActionPair", "State/Action Pair")

getTTestNumberOfSteps()
#getTTestReturnValues()
#getTTestNumberOfStates()
#getTTestNumberOfStatesActionsPair()
