import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from matplotlib.pyplot import xlabel, ylabel
from matplotlib.font_manager import FontProperties

import numpy as np
import pickle
from os import popen
from numpy.lib.function_base import average
from fileinput import close
from code import interact

numberOfTrials = 30

splitCharacter = "-"
#splitCharacter = ""

allFoldersNames = list()

#allFoldersNames.append("testes/")
#allFoldersNames.append("testes2-haha/")


##############Graphs Done #####################################

#Rope Ladder Complete
allFoldersNames.append("RopeLadderCompleto-oASPMDP/")
allFoldersNames.append("RopeLadderCompleto-oASPMDPHeuristic/")
#allFoldersNames.append("RopeLadderCompleto-QLearning/")
allFoldersNames.append("RopeLadderCompleto-QLearningHeuristic/")
allFoldersNames.append("Teste-QLearningHeuristic/")

#FishermansFolly Deterministic Complete
'''allFoldersNames.append("FishermansFollyCompletoDeterministic-oASPMDP/")
allFoldersNames.append("FishermansFollyCompletoDeterministic-oASPMDPHeuristic/")
allFoldersNames.append("FishermansFollyCompletoDeterministic-QLearning/")
allFoldersNames.append("FishermansFollyCompletoDeterministic-QLearningHeuristic/")'''

#FishermansFolly Deterministic Simplified
'''allFoldersNames.append("FishermansFollySimplifiedDeterministic-oASPMDP/")
allFoldersNames.append("FishermansFollySimplifiedDeterministic-QLearning/")'''

#FishermansFolly NonDeterministic Complete
'''allFoldersNames.append("FishermansFollyNonDeterministic-oASPMDP/")
allFoldersNames.append("FishermansFollyNonDeterministic-oASPMDPHeuristic/")
allFoldersNames.append("FishermansFollyNonDeterministic-QLearning/")
allFoldersNames.append("FishermansFollyNonDeterministic-QLearningHeuristic/")'''

#FishermansFolly NonDeterministic Simplified
'''allFoldersNames.append("FishermansFollySimplifiedNonDeterministic-oASPMDP/")
allFoldersNames.append("FishermansFollySimplifiedNonDeterministic-QLearning/")'''

#FishermansFolly NonStationary Sphere
'''allFoldersNames.append("FishermansFollyNonStationarySphere-oASPMDP/")
allFoldersNames.append("FishermansFollyNonStationarySphere-QLearning/")'''

#FishermansFolly NonStationary Disk
'''allFoldersNames.append("FishermansFollyNonStationaryDisk-oASPMDP/")
allFoldersNames.append("FishermansFollyNonStationaryDisk-QLearning/")'''





############# OLDDDDDD NAMES #################


'''allFoldersNames.append("01Data-QLearning/") 
allFoldersNames.append("02Data-oASPMDP/")
allFoldersNames.append("03Data-QLearningHeuristic/")
allFoldersNames.append("04Data-oASPMDPHeuristic/")'''

#allFoldersNames.append("testes/")
#allFoldersNames.append("04Data-oASPMDPHeuristic28Actions/")

#Rope Ladder completo
'''allFoldersNames.append("01Data-QLearning28Actions2/") 
allFoldersNames.append("02Data-oASPMDP28Actions2/")
allFoldersNames.append("03Data-QLearningHeuristic28Actions2/")
allFoldersNames.append("04Data-oASPMDPHeuristic28Actions4/")'''


#Fishermans Folly com nós possíveis, deterministico
'''allFoldersNames.append("FishermansDeterministic-QLearning/")
allFoldersNames.append("FishermansDeterministic-oASPMDP/")'''


#Resultados muito ruins para oASPMDP com Heurística
'''allFoldersNames.append("04Data-oASPMDPHeuristic28Actions2/")
allFoldersNames.append("04Data-oASPMDPHeuristic28Actions3/")'''


#allFoldersNames.append("testes/")
#allFoldersNames.append("testes2/")
'''allFoldersNames.append("oASPMDPHeuristic/")
allFoldersNames.append("oASPMDP/")
allFoldersNames.append("Q-Learning/")
allFoldersNames.append("QLearningHeuristic/")
'''

#allFoldersNames.append("FishermansNonDeterministic-QLearning/")
#allFoldersNames.append("FishermansNonDeterministic-QLearning2/")
#allFoldersNames.append("FishermansNonDeterministic-oASPMDP4/")
'''allFoldersNames.append("FishermansNonDeterministic-QLearning3/")
allFoldersNames.append("FishermansNonDeterministicSimplified-QLearning/")'''

#Todos resultados ruins para oASP(MDP) e Não Determinístico
'''allFoldersNames.append("FishermansNonDeterministic-oASPMDP2/")
allFoldersNames.append("FishermansNonDeterministic-oASPMDP/")
allFoldersNames.append("FishermansNonDeterministic-oASPMDP3/")
allFoldersNames.append("FishermansNonDeterministic-oASPMDP4/")'''

#allFoldersNames.append("FishermansNonDeterministic-oASPMDP3/")
#allFoldersNames.append("FishermansNonStationary-oASPMDP/")

#allFoldersNames.append("FishermansNonStationaryEsfera-QLearning/")
#allFoldersNames.append("FishermansNonStationary-oASPMDP/")
#allFoldersNames.append("FishermansNonStationaryDisk-QLearning/")

'''allFoldersNames.append("FishermansNonDeterministic-oASPMDP5/")
allFoldersNames.append("FishermansNonDeterministic-QLearning2/")
allFoldersNames.append("FishermansNonDeterministic-QLearning3/")
'''



#allFoldersNames.append("FishermansNonDeterministic-QLearning/")

def mountGraph(allData, graphLabel, listOfGraphLegends, graphNumber, applyZoom, xLabel = "X", yLabel = "Y"):
    graphNameCount = 0
    fig, graph = plt.subplots()
    plt.xlabel(xLabel, fontsize=20)
    if(graphNumber == 2):
        plt.ylabel(yLabel, fontsize=20, labelpad = -2)
    elif(graphNumber == 4):
        plt.ylabel(yLabel, fontsize=20, labelpad = -4)
    else:
        plt.ylabel(yLabel, fontsize=20)
    plt.title(graphLabel, fontsize=20)

    lineStyles = ['-', '-', '-', '-']
    CB_color_cycle = ['#DE8F05', '#0173B2', '#949494', '#CC78BC']


    if(applyZoom):
        zoomedInGraph = zoomed_inset_axes(graph, 2.5, loc=5)  # zoom-factor: 2.5, location: middle right

    for data in allData:        
        meanValues, stdvValues = data
        
        tamanhoVerdadeiro = len(meanValues) #* 10

        numberOfDataPoints = 0

        if(graphNumber == 1):
            tamanhoVerdadeiro = len(meanValues) * 10
            numberOfDataPoints = range(0, tamanhoVerdadeiro, 10)
        else:        
            meanValues = meanValues[::10]
            stdvValues = stdvValues[::10]
            numberOfDataPoints = range(0, tamanhoVerdadeiro, 10)

        
        #meanValues = meanValues[:5000:]
        #stdvValues = stdvValues[:2000:]
        #numberOfDataPoints = range(len(meanValues))
        #plt.yscale("log")

        # Include stdvValues as the third argument, if you want to show the standard deviation values
        #plt.errorbar(numberOfDataPoints, meanValues, label=listOfGraphLegends[graphNameCount])
        graph.errorbar(numberOfDataPoints, meanValues, label=listOfGraphLegends[graphNameCount],
                       lineStyle=lineStyles[graphNameCount], color=CB_color_cycle[graphNameCount], ms=40, markeredgewidth=10)

        if(applyZoom):
            #Setting up the zoomed in graph:
            zoomedInGraph.plot(numberOfDataPoints, meanValues, lineStyle=lineStyles[graphNameCount], color=CB_color_cycle[graphNameCount])

            #x1, x2, y1, y2 = 0, 1500, 0, 100  # specify the limits of the zoom for the Simplified Fishermans Folly (Number of Steps)
            #x1, x2, y1, y2 = 2200, 3300, 0, 100  # specify the limits of the zoom for the NonStationary Fishermans Disk (Number of Steps)
            #x1, x2, y1, y2 = 2200, 3300, -4000, 1100  # specify the limits of the zoom for the NonStationary Fishermans Disk (Accumulated Return)
            #x1, x2, y1, y2 = 0, 2000, 0, 80  # specify the limits of the zoom for the Original Fishermans Folly (Number of Steps)
            #x1, x2, y1, y2 = 0, 1500, 0, 80  # specify the limits of the zoom for the NonDeterministic Fishermans Folly (Number of Steps)
            x1, x2, y1, y2 = 600, 2100, 10, 90  # specify the limits of the zoom for the RopeLadder (Number of Steps)

            zoomedInGraph.set_xlim(x1, x2)  # apply the x-limits
            zoomedInGraph.set_ylim(y1, y2)  # apply the y-limits

            # If True, then it shows the values for the legend in the zoomed in graph. Otherwise, it doesn't show.
            plt.yticks(visible=True)
            plt.xticks(visible=True)
            # End of zoomed in graph configuration.

        graphNameCount += 1


    if(applyZoom):
        #Plotting the zoomed in graph.
        zoomedInGraph.xaxis.label.set_text("")
        zoomedInGraph.yaxis.label.set_text("")
        # ec is the line color
        mark_inset(graph, zoomedInGraph, loc1=2, loc2=3, fc="none", ec="0.3")

    '''fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 12 #width
    fig_size[1] = 9 #heigth
    plt.rcParams["figure.figsize"] = fig_size'''

    if(graphNumber == 1):
        plt.ylim(ymax=5)
    
    
    if(graphNumber < 2):
        graph.legend(bbox_to_anchor=(1.0, 1), loc=1, borderaxespad=0., prop={'size': 15}) #Show the legend on the top of the image
    else:
        #graph.legend(loc=4, borderaxespad=0., bbox_to_anchor=(1.0, 0.5), prop={'size': 15}) #Show the legend on the bottom of the image. If want to change in the vertical, change the second parameter in bbox_to_anchor
        graph.legend(loc=5, prop={'size': 15})  # Show the legend on the bottom of the image

    plt.savefig(str(graphNumber) + '.png')
    plt.show()

    
def mountGraphTest(allData, graphLabel, listOfGraphLegends, xLabel = "X", yLabel = "Y"):
        
    graphNameCount = 0
    for data in allData:        
        meanValues = data
        
        tamanhoVerdadeiro = len(meanValues) * 10
        
        meanValues = meanValues[::10]
        
        numberOfDataPoints = range(0, tamanhoVerdadeiro, 100)
        
        #numberOfDataPoints = range(len(meanValues))
        #Include stdvValues as the third argument, if you want to show the standard deviation values
        #plt.yscale("log")
        plt.errorbar(numberOfDataPoints, meanValues)
        graphNameCount += 1

    
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)

    #plt.ylim(ymax=4)
    
    fontP = FontProperties()
    fontP.set_size('small')

    plt.title(graphLabel)
    #plt.legend(prop=fontP)
    plt.legend(bbox_to_anchor=(0.45, 1), loc=1, borderaxespad=0., )
    #plt.savefig('stateAction' + '.png')
    plt.show()
    
    
def getValues(baseFileName, folderName):
    listOfValues = list()
    #baseFileName = "02Data-oASPMDP/" + baseFileName
    baseFileName = folderName + baseFileName
    
    for trial in range(0, numberOfTrials):
        '''if(folderName == "01Data-QLearning/"):
            fullFileName = baseFileName + "-" + str(trial+14) + ".pck"
            listOfValues.append(pickle.load(open(fullFileName, "rb")))
        else:
            fullFileName = baseFileName + "-" + str(trial+1) + ".pck"
            listOfValues.append(pickle.load(open(fullFileName, "rb")))
	'''

        fullFileName = baseFileName + "-" + str(trial+1) + ".pck"
        listOfValues.append(pickle.load(open(fullFileName, "rb")))
        
    return listOfValues

def getValuesByTrial(baseFileName, folderName, currentTrial):
    listOfValues = list()
    baseFileName = folderName + baseFileName
    
    fullFileName = baseFileName + "-" + str(currentTrial+1) + ".pck"
    listOfValues.append(pickle.load(open(fullFileName, "rb")))
    
    return listOfValues

def getStatisticalValues(listOfValues, numberOfDataPoints):
    averageValuesAux = list()
    standardDeviationValuesAux = list()
        
    for i in range(0, numberOfDataPoints):
        valuesCombined = list()
        for j in range(0, numberOfTrials):
            valuesCombined.append(listOfValues[j][i])
        
        averageValuesAux.append(np.mean(valuesCombined))
        standardDeviationValuesAux.append(np.std(valuesCombined, axis = 0, ddof=1))
    
    valuesToShow = (averageValuesAux, standardDeviationValuesAux)
    return valuesToShow


def numberOfStateActionPairs(graphNumber):
    baseFileName = "stateActionPair"
    allData = list()
    listOfNames = list()
    
    for folderName in allFoldersNames:
        '''allLoadedValues = getValues(baseFileName, folderName)
        numberOfDataPoints = len(allLoadedValues[0])
        
        numberOfStatesActions = list()
        for trial in allLoadedValues:
            numberOfStatesActionsAux = list()
            for interaction in trial:
                numberOfActionsInState = 0
                
                for state in interaction:
                    numberOfActionsInState += len(interaction[state])
                
                numberOfStatesActionsAux.append(numberOfActionsInState)
                
            numberOfStatesActions.append(numberOfStatesActionsAux)
        '''
            
        '''numberOfStatesActions = list()
        numberOfDataPoints = 0
            
        for currentTrial in range(0, numberOfTrials):
            currentLoadedValues = getValuesByTrial(baseFileName, folderName, currentTrial)
            numberOfStatesActionsAux = list()
            numberOfDataPoints = len(currentLoadedValues[0])

            for interaction in currentLoadedValues[0]:
                numberOfActionsInState = 0
                
                for state in interaction:
                    numberOfActionsInState += len(interaction[state])
                    
                numberOfStatesActionsAux.append(numberOfActionsInState)
            
                     
            numberOfStatesActions.append(numberOfStatesActionsAux)
         
        
        statiscalValues = getStatisticalValues(numberOfStatesActions, numberOfDataPoints)
        allData.append(statiscalValues)'''
            
        numberOfStates = getValues(baseFileName, folderName)
        numberOfDataPoints = len(numberOfStates[0]) 
        statiscalValues = getStatisticalValues(numberOfStates, numberOfDataPoints)
        allData.append(statiscalValues)
        if(splitCharacter != ""):
            name = folderName.split(splitCharacter)[1]
            listOfNames.append(name)
        else:
            listOfNames.append("Teste") 
         
    graphLabel = "State/Action Pair"
    xLabel = "Interaction"
    yLabel = "Pairs"
    applyZoom = False
    mountGraph(allData, graphLabel, listOfNames, graphNumber, applyZoom, xLabel, yLabel)


def numberOfStatesGraph(graphNumber):
    baseFileName = "qsa"
    allData = list()
    listOfNames = list()
    
    for folderName in allFoldersNames:
        #allLoadedValues = getValues(baseFileName, folderName)
        #numberOfDataPoints = len(allLoadedValues[0])        
        
        #numberOfStates = list()
        #numberOfDataPoints = 0
        
        '''allLoadedValues = getValues(baseFileName, folderName)
        numberOfDataPoints = len(allLoadedValues[0])
        print(numberOfDataPoints)
        
        numberOfStates = list()
        count = 0
        for trial in allLoadedValues:
            allLoadedValues[count] = list()
            numberOfStatesAux = list()
            for interaction in trial:
                numberOfStatesAux.append(len(interaction))
        
            numberOfStates.append(numberOfStatesAux)
        '''    
        
        
        '''for currentTrial in range(0, numberOfTrials):
            currentLoadedValues = getValuesByTrial(baseFileName, folderName, currentTrial)
            numberOfStatesAux = list()
            numberOfDataPoints = len(currentLoadedValues[0])
            
            for interaction in currentLoadedValues[0]:
                numberOfStatesAux.append(len(interaction))
                input()
            
                     
            numberOfStates.append(numberOfStatesAux)
            
            
        statiscalValues = getStatisticalValues(numberOfStates, numberOfDataPoints)
        allData.append(statiscalValues)'''
            
        numberOfStates = getValues(baseFileName, folderName)
        numberOfDataPoints = len(numberOfStates[0]) 
        statiscalValues = getStatisticalValues(numberOfStates, numberOfDataPoints)
        allData.append(statiscalValues)
        if(splitCharacter != ""):
            name = folderName.split(splitCharacter)[1]
            listOfNames.append(name)
        else:
            listOfNames.append("Teste")      
         
    graphLabel = "States Visited"
    xLabel = "Interaction"
    yLabel = "Number of States"
    applyZoom = False
    mountGraph(allData, graphLabel, listOfNames, graphNumber, applyZoom, xLabel, yLabel)

def returnsValuesGraph(graphNumber):
    baseFileName = "ret"
    allData = list()
    listOfNames = list()
    
    for folderName in allFoldersNames:
        returnsValues = getValues(baseFileName, folderName)
        '''for returns in returnsValues:
            print(returns)'''
        numberOfDataPoints = len(returnsValues[0]) 
        statiscalValues = getStatisticalValues(returnsValues, numberOfDataPoints)
        allData.append(statiscalValues)
        if(splitCharacter != ""):
            name = folderName.split(splitCharacter)[1]
            listOfNames.append(name)
        else:
            listOfNames.append("Teste")  
    
    graphLabel = "Accumulated Return"
    xLabel = "Interaction"
    yLabel = "Return Value"
    applyZoom = False
    mountGraph(allData, graphLabel, listOfNames, graphNumber, applyZoom, xLabel, yLabel)

def allReturnsTest(graphNumber):
    baseFileName = "ret"
    allData = list()
    listOfNames = list()
    listOfValues = list()
    folderName = "04Data-oASPMDPHeuristic28Actions/"
    
    baseFileName = "04Data-oASPMDPHeuristic28Actions/" + baseFileName
    for trial in range(0, numberOfTrials):
        fullFileName = baseFileName + "-" + str(trial+1) + ".pck"
        returnValues = pickle.load(open(fullFileName, "rb"))
        allData.append((returnValues))
    
            
    if(splitCharacter != ""):
        name = folderName.split(splitCharacter)[1]
        listOfNames.append(name)
    else:
        listOfNames.append("Teste")  
    
    graphLabel = "Accumulated Return"
    xLabel = "Interaction"
    yLabel = "Return Value"
    mountGraphTest(allData, graphLabel, listOfNames, graphNumber, xLabel, yLabel)
    
def diffQGraph(graphNumber):
    baseFileName = "diffQ"
    allData = list()
    listOfNames = list()
    
    for folderName in allFoldersNames:
        diffQ = getValues(baseFileName, folderName)
        numberOfDataPoints = len(diffQ[0]) 
        statiscalValues = getStatisticalValues(diffQ, numberOfDataPoints)
        allData.append(statiscalValues)
        if(splitCharacter != ""):
            name = folderName.split(splitCharacter)[1]
            listOfNames.append(name)
        else:
            listOfNames.append("Teste")
    
    
    graphLabel = "RMSD"
    xLabel = "Interaction"
    yLabel = "Q Difference"
    applyZoom = False
    mountGraph(allData, graphLabel, listOfNames, graphNumber, applyZoom, xLabel, yLabel)

def numberOfStepsGraph(graphNumber): 
    baseFileName = "steps"
    allData = list()
    listOfNames = list()
    
    for folderName in allFoldersNames:
        numberOfSteps = getValues(baseFileName, folderName)
        numberOfDataPoints = len(numberOfSteps[0]) 
        statiscalValues = getStatisticalValues(numberOfSteps, numberOfDataPoints)
        allData.append(statiscalValues)
        if(splitCharacter != ""):
            name = folderName.split(splitCharacter)[1]
            listOfNames.append(name)
        else:
            listOfNames.append("Teste")
    
    graphLabel = "Number of Steps"
    xLabel = "Interaction"
    yLabel = "Steps"
    applyZoom = True
    mountGraph(allData, graphLabel, listOfNames, graphNumber, applyZoom, xLabel, yLabel)
    
    
    
#allReturnsTest()
numberOfStepsGraph(0)
diffQGraph(1)
returnsValuesGraph(2)
numberOfStatesGraph(3)
numberOfStateActionPairs(4)
