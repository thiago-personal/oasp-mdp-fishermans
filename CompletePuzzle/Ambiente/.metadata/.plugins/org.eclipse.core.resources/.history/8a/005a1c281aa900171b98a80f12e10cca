###############################################################################

## Imports
from Element import Element
from enumerations import ElementType, ElementFace, Actions
from ElementsRelation import ElementsRelation


# Creating the elements of the puzzle.
post = Element(ElementType.POST)
mainString = Element(ElementType.MAINSTRING)
ring = Element(ElementType.RING)
sphere1 = Element(ElementType.SPHERE1)
sphere2 = Element(ElementType.SPHERE2)
disk1 = Element(ElementType.DISK1)
disk2 = Element(ElementType.DISK2)


# Initial State
# For now the initial state is going to be a constant state, pre-defined by the programmer.
# Creating Initial Relationships

global stringRelations
stringRelations = [ElementsRelation(ElementType.MAINSTRING, sphere1.elementType, ElementFace.POSITIVE),
                   ElementsRelation(ElementType.MAINSTRING, post.elementType, ElementFace.POSITIVE),
                   ElementsRelation(ElementType.MAINSTRING, sphere2.elementType, ElementFace.POSITIVE)]

global postRelations
postRelations = [ElementsRelation(ElementType.POST, ring.elementType, ElementFace.POSITIVE)]

initialState =  (stringRelations.copy(), postRelations.copy())

# Goal State
# The Goal State is a state where the Ring isn't being crossed by any other element.
def isGoalState():
    '''for relation in stringRelations:
        if relation.element == ElementType.RING:
            return False #If the ring is being crossed by the String, then it's not the goal state.
    '''
    for relation in postRelations:
        if relation.element == ElementType.RING: 
            return False #If the ring is being crossed by the Post, then it's not the goal state.
        
    #If the ring isn't being crossed by the String nor the Post, then it's free from the system.
    return True


###############################################################################
# action set
# Possible Actions:
# 0 - Pass Disk through the Post Hole.
# 1 - Pass String through the Ring.
# 2 - Pass Sphere through the Ring.
# 3 - Take off the Ring from the Post.
# 4 - Take off the Ring from the String.
# 5 - Pass Post through the Ring.

actions = [Actions.DISK1THROUGHPOST, Actions.STRINGTHROUGHRING, Actions.SPHERE1THROUGHRING,
           Actions.TAKERINGFRIOMPOST, Actions.TAKERINGFROMSTRING, Actions.POSTTHROUGHRING,
           Actions.DISK2THROUGHPOST, Actions.SPHERE2THROUGHRING]

###############################################################################
# transition probabilities
oposite = 0
orthopos = 0
orthoneg = 0

aO = [0, 0, 0]
aP = [0, 0, 0]
aN = [0, 0, 0]

###############################################################################
# reward values
minimalReward  = -100
maximumReward  = 100
rewardPerStep = -1 #maybe change to -4. It's a good idea to run tests.

###############################################################################
episodes = 250
trials = 1
port = 5000

###############################################################################
