from enumerations import ElementType, ElementFace
from Elements import *

class ElementsRelation:
    
    def __init__(self, elementCrossing, elementBeingCrossed, face):
        self.elementCrossing = elementCrossing
        self.elementBeingCrossed = elementBeingCrossed
        self.face = face #The natural face of the elements being crossed is the way they are in the puzzle. It's intuitive, the exit of the element gives the face.
        
    def printRelationship(self):
        print("The " + self.elementBeingCrossed.nameOfElement + 
              " with the face: " + ElementFace(self.face) +
              " is being crossed by the: " + self.elementCrossing.nameOfElement)
    
    