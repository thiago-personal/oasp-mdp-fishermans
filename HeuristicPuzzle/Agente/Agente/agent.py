
# for printing Q and P tables
import pprint
pp = pprint.PrettyPrinter(depth=100)

# random methods for action selection and state initialization
from random import random
from random import choice


# learning variables
from exp import *
import exp

# online methods of oASP(MDP)
import online

# ASP methods of oASP(MDP)
import asp

# MDP methods of oASP(MDP)
import mdp

# episode variables
tSteps = 0   # total steps in a episode
actions = [] # actions list

maximumReward = 0     # maximum value for reward
minimumReward = 0     # minimum value for reward

forbiddenStates = []  # list of forbidden states
forbiddenActions = []  # list of forbidden actions
forbiddenActionState = [] # list of forbidden state-action pairs
goalState = []  # list of goal states

################################################################################
def saveData(qs, diffQ, steps, ret, currentQTable, trial):
    from pickle import dump
    from os import popen

    fname = 'diffQ-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(diffQ, f)

    fname = 'steps-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(steps, f)

    fname = 'ret-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(ret, f)

    fname = 'qsa-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(qs, f)
        
    fname = 'output-' + str(trial) + '.txt'
    text_file = open(fname, "w")
    text_file.write("*"*30)
    text_file.write("\n\nQ(s,a)")
    text_file.write(str(currentQTable))
    text_file.write("*"*30)
    text_file.write("\n\n\n Number of States: " + str(len(currentQTable)))
    text_file.close()
    print("Number of States: " + str(len(currentQTable)))
    

    popen('rm *.lp')


################################################################################
def episode(qTable, pTable, updatedEpsilon):

    global tSteps
    global minimumReward, maximumReward, actions

    # get connection to environment
    socket = online.initSocket(port)

    # receive env information
    maximumReward, minimumReward, actions = online.getConf(socket)

    # save actions for ASP calls
    if RUN_ASP:
        asp.saveActionList(actions, forbiddenActions)

    # agent's initial configuration
    newState, end = online.initState(socket)

    # variables to save
    step = 0
    returnReward = 0

    somethingChanged = False
    changedStates = []

    # here is the oASP(MDP)
    while not end:

        ### ONLINE
        # update state
        currentState = newState
        
        
        # choose action
        actionToPerform = mdp.chooseAction(qTable, actions, currentState, choice, forbiddenActions, forbiddenActionState, updatedEpsilon)

        # perform action in the environment
        online.sendMessage(currentState, actionToPerform, socket)

        # receive information from environment
        newState, reward, end, forbiddenActionPerformed = online.receiveMessage(socket)

        ### END ONLINE

        # UPDATE Q AND P FUNCTIONS AND ASP FILES
        changes = asp.updateASPfiles(qTable, pTable,
                currentState, actionToPerform, newState,
                reward, minimumReward, maximumReward,
                goalState, forbiddenStates, forbiddenActionState, forbiddenActionPerformed, forbiddenActions)
        somethingChanged = somethingChanged or changes
        # END UPDATE

        ### MDP
        # Q(s,a) update

        qTable[currentState][actionToPerform] = mdp.updateQvalue(qTable, currentState, 
                                                                 actionToPerform, reward, newState)

        
        # variables to send
        step += 1
        returnReward += reward 

        tSteps += 1
        if step == maxSteps:
            end = True
        ### END MDP

        ### ASP CALL
        if RUN_ASP:
            if changes:
                asp.saveStateDesc(pTable, currentState)
                changedStates.append(currentState)

        ### END ASP CALL

        #if DEBUG: input("Press anykey to continue")


    online.sendEndEpisode(socket)

    if somethingChanged:
        if RUN_ASP:
            newQTableFromASP = asp.genNewQ(pTable, changedStates)
            qTable = mdp.updateQwithASP(qTable, newQTableFromASP)

        if DEBUG: pp.pprint(newQTableFromASP)
        

    return qTable, pTable, step, returnReward


#Return a new value for Epsilon.
def updateEpsilon(currentEpisode, currentEpsilon):
    if((currentEpisode % exp.stepToUpdateEpsilon) == 0 and currentEpisode != 0):
        currentEpsilon = round(currentEpsilon, 1)
        if(currentEpsilon > 0.1):
            return (currentEpsilon - 0.1) #The epsilon value decreases 10% at each 85interactions. 

    return currentEpsilon

################################################################################
def experiment():
    updatedEpsilon = exp.initialEpsilon
    
    for trial in range(trials):
        print("Trial: %s" % int(trial+1))
        pTable = dict()
        qTable = dict()

        # variables to be saved
        qTablesList = list()
        steps = list()
        returns = list()
        diffQ = list()
        oldQTable = qTable.copy()

        currentEpisode = 0
        #for currentEpisode in range(nenvs * episodes):
        for currentEpisode in range(episodes):
            #print("Episode: %s" % int(currentEpisode))
            qTable, pTable, step, reward = episode(qTable, pTable, updatedEpsilon)

            #Values that are going to be used to build graphs and make comparisons.
            if((currentEpisode+1) % window == 0):
                differenceBetweenQs = mdp.rmsd(oldQTable, qTable)
                oldQTable = qTable.copy()
                qTablesList.append(oldQTable)
                steps.append(step)
                returns.append(reward)
                diffQ.append(differenceBetweenQs)
            
            #updatedEpsilon = updateEpsilon(currentEpisode, updatedEpsilon)
                
        
        saveData(qTablesList, diffQ, steps, returns, qTable, trial+1)
        
    print("\n\n")
    print("Tabela Final " + str(qTable))


################################################################################
import time
start_time = time.time()
experiment()
print("--- %s Minutes ---" % ((time.time() - start_time) / 60))
