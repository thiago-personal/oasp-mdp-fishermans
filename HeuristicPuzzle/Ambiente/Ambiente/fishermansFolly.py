### Fisherman's Folly Puzzle

#Imports
from enviromentDefinition import *
from random import random
import zmq
import enviromentDefinition
from enumerations import ElementFace, ElementType
from ElementsRelation import ElementsRelation

################################################################################
def nextState(state, action):
    terminalState = False
    reward  = rewardPerStep
    forbiddenStateActionPerformed = False
    forbiddenActionPerformed = False
    
    #Getting the mapped state and casting to the complex state, with the two chain lists.
    currentState = mapFromStringToState(state)
    #strintRelations, postRelations, ringRelations = currentState
    
    
    entitiesOfAction = getDescriptionOfAction(action)
    hostElement, elementReceivingAction, elementFace = entitiesOfAction
    
    resultOfExecutedAction = hostElement.determineActionToExecute(elementReceivingAction, elementFace, currentState) 
    forbiddenStateActionPerformed, newState, forbiddenActionPerformed = resultOfExecutedAction 
    
    #newState = (stringRelations.copy(), postRelations.copy(), ringRelations.copy())
    mappedNewState = mapStateToString(newState)
    #print("New state mapped: " + mappedNewState)
    
    if (forbiddenStateActionPerformed or forbiddenActionPerformed):
        reward = minimalReward 
        
    elif isGoalState(newState):
        reward = maximumReward
        terminalState = True
        
    #showInformationAboutRelations(newState)
    
    return mappedNewState, reward, terminalState, forbiddenActionPerformed   

################################################################################
def mapStateToString(stateToMap):
    mappedState = ""
    
    #Separating the two list of crossings, one for each element of the system.
    stringCrossings, postCrossings = stateToMap 
    
    for elementRelationToConvert in stringCrossings:
        mappedState += getElementStateRepresentation(elementRelationToConvert)
    
    #mappedState += "-"
    mappedState += separador
    
    for elementRelationToConvert in postCrossings:
        mappedState += getElementStateRepresentation(elementRelationToConvert)
                
    
    return mappedState

################################################################################
def mapFromStringToState(mappedState):
    #print(mappedState)
    #stringCrossingsMapped, postCrossingsMapped, ringCrossingsMapped = mappedState.split("-")
    stringCrossingsMapped, postCrossingsMapped = mappedState.split(separador)
    
    stringCrossings = getComplexRelationReprsentation(stringCrossingsMapped, mainString)
    postCrossings = getComplexRelationReprsentation(postCrossingsMapped, post)
    
    complexState = (stringCrossings, postCrossings)
    
    return complexState


################################################################################
def getElementStateRepresentation(elementRelationToConvert):
    #Getting the int value of the enumeration that represents the element that is crossing the string or the post.
    #print(str(elementRelationToConvert.element) + " " + str(elementRelationToConvert.face) + " " + str(elementRelationToConvert.parent))
    
    elementConverted = ""
    
    if elementRelationToConvert.face == ElementFace.POSITIVE:
        elementConverted = "p"
    else:
        elementConverted = "n"
        
    elementConverted += str(elementRelationToConvert.elementInChain.orderInPuzzle)
        
    return elementConverted   

################################################################################
def getComplexRelationReprsentation(mappedStateToConvert, hostElement):
    numberOfPositionsToSplit = 2
    listOfElementsRelationMapped = [mappedStateToConvert[i:i+numberOfPositionsToSplit] 
                                   for i in range(0, len(mappedStateToConvert), numberOfPositionsToSplit)]
    elementChain = list()
    
    for relation in listOfElementsRelationMapped:
        face = ElementFace.POSITIVE if relation[0] == "p" else ElementFace.NEGATIVE
        elementInChain = getElementObjectFromInteger(int(relation[1]))
        
        elementChain.append(ChainRelations(hostElement, elementInChain, face))
        
    return elementChain


################################################################################
def getElementObjectFromInteger(orderOfElementInPuzzle):
    if (orderOfElementInPuzzle == 0):
        return post
    elif (orderOfElementInPuzzle == 1):
        return mainString
    elif (orderOfElementInPuzzle == 2):
        return ring
    elif (orderOfElementInPuzzle == 3):
        return disk1
    elif (orderOfElementInPuzzle == 4):
        return disk2
    elif (orderOfElementInPuzzle == 5):
        return sphere1
    else:
        return sphere2
    

################################################################################
def getDescriptionOfAction(action):
    actionDescription = actionsDescription[action]
    
    hostElement = getElementInActionDescription(actionDescription, False)
    elementInChain = getElementInActionDescription(actionDescription, True)
    elementFace = getFaceInActionDescription(actionDescription)
    
    completeDescriptionOfAction = (hostElement, elementInChain, elementFace)
    return completeDescriptionOfAction


################################################################################
def getElementInActionDescription(actionDescription, isElementBeingCrossed):
    indexToGetElement = 0
    #By default it gets the first position of the string because it's the element crossing, but if it's the other element, then it has to get the index 1
    if (isElementBeingCrossed):
        indexToGetElement = 1
    
    element = int(actionDescription[indexToGetElement])
    
    return getElementObjectFromInteger(element)
    
    
################################################################################    
def getFaceInActionDescription(actionDescription):
    face = int(actionDescription[2])
    
    if(face == 0):
        return ElementFace.NEGATIVE
    else:
        return ElementFace.POSITIVE
    

################################################################################
def initState():
    #message = 'InitialState'
    message = mapStateToString(initialState)
    return message


################################################################################
def envConf():
    message = ''

    for action in actions:
        message += str(action) + ' '

    message += str(maximumReward) + ' ' + str(minimalReward)

    return message


################################################################################
def stationaryEnvironment(socket, maxEpisodes):
    print("FisherMan's Folly Puzzle")

    e = 0
    for _ in range(maxEpisodes):
        end = False

        while not end:
            message = socket.recv_string()
            #print("Phase: %s" % message)

            if message == 'init':
                message = initState()

            elif message == 'conf':
                message = envConf()
                e += 1

            elif message == 'END':
                end = True

            else:
                #The action to be performed came from the agent. The agent code decides the action.
                state, action = message.split()
                action = int(action)

                newState, reward, terminalState, forbiddenActionPerformed = nextState(state, action)
                
                message = str(newState) + ' '
                message += str(reward) + ' '
                message += str(terminalState) + ' '
                message += str(forbiddenActionPerformed)

            #print("About this execution: %s" % message)
            socket.send_string(message)

################################################################################
def runEnvironment(episodes, trials):
    import zmq
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    conn = "tcp://*:" + str(port)
    socket.bind(conn)
    print("Environment is up")

    #global postRelations, stringRelations

    for t in range(trials):
        print("Trial: ", t)
        stringRelations = initialState[0]
        postRelations = initialState[1]
        stationaryEnvironment(socket, episodes)


################################################################################
if __name__ == '__main__':
    runEnvironment(episodes, trials)


################################################################################
