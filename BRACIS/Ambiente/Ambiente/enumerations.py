from enum import Enum
    
class ElementType(Enum):
    POST = 1
    MAINSTRING = 2
    RING = 3
    DISK = 4
    SPHERE = 5
    
class Actions(Enum):
    CROSS = 0
    UNDOCROSS = 1 #Action not being used.
    
class ElementFace(Enum):
    NEGATIVE = 0
    POSITIVE = 1
