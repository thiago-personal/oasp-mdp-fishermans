## Imports
from Element import Element
from enumerations import ElementType, ElementFace, Actions
from ElementsRelation import ElementsRelation
from Elements import *
from Disk import *
from MainString import *
from Post import *
from Ring import *
from Block import *
from chainRelations import ChainRelations


# Creating the elements of the puzzle.
post = Post("post", 0, 0)
mainString = MainString("str", 10, 1)
ring = Ring("ring", 20, 2)
disk = Disk("disk", 30, 3)
block = Block("block", 40, 4)

# Initial State
# For now the initial state is going to be a constant state, pre-defined by the programmer.
# Creating Initial Relationships

stringRelations = [ChainRelations(mainString, mainString, ElementFace.NEGATIVE),
                   ChainRelations(mainString, post, ElementFace.POSITIVE),
                   ChainRelations(mainString, mainString, ElementFace.POSITIVE)]

#Post is crossing the Ring.
postRelations = [ChainRelations(post, post, ElementFace.NEGATIVE),
                 ChainRelations(post, ring, ElementFace.POSITIVE),
                 ChainRelations(post, post, ElementFace.POSITIVE)]

initialState =  (stringRelations.copy(), postRelations.copy())
currentComplexState = (stringRelations.copy(), postRelations.copy())


# Goal State
# The Goal State is a state where the Ring isn't being crossed by any other element.
def isGoalState(completeStateToVerify):
    stringRelationsToVerify, postRelationsToVerify = completeStateToVerify
    
    for relation in stringRelationsToVerify: #In the position zero it's the string relations
        if int(relation.elementInChain.orderInPuzzle) == int(ring.orderInPuzzle):
            return False #If the ring is being crossed by the MainString, then it's not the goal state.
    
    for relation in postRelationsToVerify: #In the position one it's the post relations
        if int(relation.elementInChain.orderInPuzzle) == int(ring.orderInPuzzle): 
            return False #If the ring is being crossed by the Post, then it's not the goal state.        
    
    #If the ring isn't being crossed by the MainString nor the Post, then it's free from the system.
    return True

    
numberOfElements = 5
numberOfFaces = 2

totalNumberOfActions = numberOfElements * numberOfElements * numberOfFaces; #50 possible actions, 10 per element
    
#In this implementation the actions are a set that specifies the element that is going to cross, a element that is going to be crossed and the face of interaction.    
actions = list(range(0, totalNumberOfActions))
actionsDescription = []

def determineListOfActionsBasedOnElements():
    for i in range(0, numberOfElements):
            for a in range(0, numberOfFaces):
                for b in range(0, numberOfElements):
                    actionDescription = "" + str(i) + str(b) + str(a) + "" #The crossing element, the element being crossed and the face.
                    actionsDescription.append(actionDescription)                    
                    
determineListOfActionsBasedOnElements()


###############################################################################
# transition probabilities
oposite = 0
orthopos = 0
orthoneg = 0

aO = [0, 0, 0]
aP = [0, 0, 0]
aN = [0, 0, 0]

###############################################################################
# reward values
minimalReward  = -100
maximumReward  = 100
rewardPerStep = -5 #maybe change to -4. It's a good idea to run tests.

###############################################################################
episodes = 250
trials = 1
port = 5000

###############################################################################
separador = "b"


###############################################################################
puzzleDefinitionProlog = "Oracle/bracisPuzzleDefinition.pl"
puzzlePlannerProlog = "Oracle/bracisPuzzlePlanner.pl"