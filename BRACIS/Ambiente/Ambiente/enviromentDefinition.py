###############################################################################

## Imports
from enumerations import  ElementFace
from Elements.Disk import *
from Elements.MainString import *
from Elements.Post import *
from Elements.Ring import *
from Elements.Sphere import *
from chainRelations import ChainRelations


# Creating the elements of the puzzle.
post = Post("post", 0, 0)
mainString = MainString("str", 14, 1)
ring = Ring("ring", 28, 2)
disk1 = Disk("disk1", 42, 3)
disk2 = Disk("disk2", 156, 4)
sphere1 = Sphere("sphere1", 70, 5)
sphere2 = Sphere("sphere2", 84, 6)


# Initial State
# For now the initial state is going to be a constant state, pre-defined by the programmer.
# Creating Initial Relationships

stringRelations = [ChainRelations(mainString, mainString, ElementFace.NEGATIVE),
                   ChainRelations(mainString, sphere1, ElementFace.POSITIVE),
                   ChainRelations(mainString, post, ElementFace.POSITIVE),
                   ChainRelations(mainString, sphere2, ElementFace.POSITIVE),
                   ChainRelations(mainString, mainString, ElementFace.POSITIVE)]

#Post is crossing the Ring.
postRelations = [ChainRelations(post, post, ElementFace.NEGATIVE),
                 ChainRelations(post, ring, ElementFace.POSITIVE),
                 ChainRelations(post, post, ElementFace.POSITIVE)]

initialState =  (stringRelations.copy(), postRelations.copy())
currentComplexState = (stringRelations.copy(), postRelations.copy())


# Goal State
# The Goal State is a state where the Ring isn't being crossed by any other element.
def isGoalState(completeStateToVerify):
    stringRelationsToVerify, postRelationsToVerify = completeStateToVerify
    
    for relation in stringRelationsToVerify: #In the position zero it's the string relations
        if int(relation.elementInChain.orderInPuzzle) == int(ring.orderInPuzzle):
            return False #If the ring is being crossed by the MainString, then it's not the goal state.
    
    for relation in postRelationsToVerify: #In the position one it's the post relations
        if int(relation.elementInChain.orderInPuzzle) == int(ring.orderInPuzzle): 
            return False #If the ring is being crossed by the Post, then it's not the goal state.
        
    #If the ring isn't being crossed by the MainString nor the Post, then it's free from the system.
    return True     
    
numberOfElements = 7
numberOfFaces = 2


totalNumberOfActions = numberOfElements * numberOfElements * numberOfFaces; #98 possible actions, 14 per element
    
#In this implementation the actions are a conjunct that especifies the element that is going to cross, a element that is going to be crossed, the cross or undo operation, and the face of interaction.    
actions = list(range(0, totalNumberOfActions))
actionsDescription = []

#In this implementation the actions are a set that specifies the element that is going to cross, a element that is going to be crossed and the face of interaction.    
actions = list(range(0, totalNumberOfActions))
actionsDescription = []

def determineListOfActionsBasedOnElements():
    for i in range(0, numberOfElements):
            for a in range(0, numberOfFaces):
                for b in range(0, numberOfElements):
                    actionDescription = "" + str(i) + str(b) + str(a) + "" #The crossing element, the element being crossed and the face.
                    actionsDescription.append(actionDescription)                    
                    
determineListOfActionsBasedOnElements()


###############################################################################
# transition probabilities
oposite = 0
orthopos = 0
orthoneg = 0

aO = [0, 0, 0]
aP = [0, 0, 0]
aN = [0, 0, 0]

###############################################################################
# reward values
minimalReward  = -100 #If each action in that episode is the bad one, then the total reward will be -1000.
maximumReward  = 100
rewardPerStep = -5

###############################################################################
episodes = 10000
trials = 30
port = 5000

###############################################################################
separador = "b"

###############################################################################
#This, together with the number of steps, is the limiting factor of the size of the string. With less steps, less crossings.
numberOfStringCrossingPost = 8
numberOfSphereCrossingRing = 8


###############################################################################
puzzleDefinitionProlog = "Oracle/fishermansFollyDefinition.pl"
puzzlePlannerProlog = "Oracle/fishermansFollyPlanner.pl"
