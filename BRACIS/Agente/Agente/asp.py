import asp2py
import py2asp

from exp import RUN_ASP, DETERMINISTIC


################################################################################
def updateASPfiles(qTable, pTable, currentState, action, newState,
        reward, minimumReward, maximumReward,
        goalStates, forbiddenStates, forbiddenActionState, forbiddenActionPerformed, forbiddenActions, allActions):

    somethingChanged = False
    # update restriction list

    # forbidden state-action pairs
    ## criteria: transition to the same state
    if DETERMINISTIC:
        if (currentState == newState or reward == minimumReward) and [currentState, action] not in forbiddenActionState:
            forbiddenActionState.append([currentState, action])
            
            if RUN_ASP:
                py2asp.restrictSApairs(forbiddenActionState, currentState)
            
            somethingChanged = True

    # forbidden actions
    # criteria: Received from the environment
    if(forbiddenActionPerformed):
        if action not in forbiddenActions:
            forbiddenActions.append(action)
            
            if RUN_ASP:
                py2asp.restrictActions(forbiddenActions)
            
            somethingChanged = True

    # forbidden states
    ## criteria: minimum reward
    #this code is commented because I wanted to test something first
    '''if reward == minimumReward and newState not in forbiddenStates or True:
        forbiddenStates.append(newState)
        if RUN_ASP:
            py2asp.restrictStates(forbiddenStates)
        somethingChanged = True
    '''
            
    # save goal state
    ## criteria: maximum reward
    if reward == maximumReward and newState not in goalStates:
        goalStates.append(newState)
        
        if RUN_ASP:
            py2asp.saveGoalStates(goalStates)
        
        somethingChanged = True

    # update Q and P functions
    if currentState not in pTable:
        pTable[currentState] = dict()
        somethingChanged = True

    if currentState not in qTable:
        qTable[currentState] = dict()
        somethingChanged = True
        if(not RUN_ASP):
            for actionAux in allActions:
                qTable[currentState][actionAux] = 0

    if action not in pTable[currentState]:
        pTable[currentState][action] = list()
        somethingChanged = True

    if action not in qTable[currentState]:
        qTable[currentState][action] = 0
        somethingChanged = True

    if newState not in pTable[currentState][action]:
        pTable[currentState][action].append(newState)
        somethingChanged = True

    '''
    #This update already happens in the agent code. It's doing the same thing twice
    if somethingChanged:
        if RUN_ASP:
            py2asp.psa2lp(pTable, currentState)
    '''
    
    return somethingChanged


################################################################################
def genNewQ(pTable, changedStates):
    return asp2py.genQfromLP(pTable.keys(), changedStates)


################################################################################
def saveActionList(actions, forbiddenActions):
    py2asp.saveActions(actions)
    py2asp.restrictActions(forbiddenActions)


################################################################################
def saveStateDesc(pTable, state):
    py2asp.psa2lp(pTable, state)


################################################################################
