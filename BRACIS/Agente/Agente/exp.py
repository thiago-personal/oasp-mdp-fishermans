import ast

# experiments variables
nenvs = 3
episodes = 10000
trials   = 30
maxSteps = 25

# RL vars
learningRate = 0.2
discount = 0.9 #Favor future actions, rather than recent ones.
initialEpsilon = 0.1
stepToUpdateEpsilon = 250

# Configuration variables
DEBUG = False
RUN_ASP = False
DETERMINISTIC = True
USE_HEURISTIC = False

# Heuristic Variables
heuristicValues = {} #List of heuristic values
if(USE_HEURISTIC):
    heuristicFile = open("heuristic", "r")
    stringHeuristicValues = heuristicFile.read()
    heuristicFile.close()
    heuristicValues = ast.literal_eval(stringHeuristicValues)

port = 5000
window = 10
